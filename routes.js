const routes = require('express').Router();

routes.get('/', async (req, res) => {
    return res.status(200).json({ "status": 200, "message": "NCB Sistemas Embarcados - Teste de proficiência em desenvolvimento" });
})

routes.get('/wdevfs', async (req, res) => {
    return res.status(200).json({ "status": 200, "message": "Route not available for this HTTP Method, try method POST!" });
})

routes.post('/wdevfs', async (req, res) => {
    try {
        
        // Receiving data and setting up variable
        let { operation, factor, values } = req.body, arrayResult = [], sum = 0, operations = ["reduce", "map-array-add", "map-array-mul"];

        // Validation
        if(!(typeof operation === "string") || !operation || !(typeof factor === "number") || !factor || !Array.isArray(values) || !values[0] || !values.every(value => typeof value === "number" ? true : false)) return res.status(400).json({"operation": "INVALID", "result": arrayResult });

        if(!operations.includes(operation)) return res.status(400).json({"operation": "INVALID", "result": arrayResult });

        // Factory
        let mathOperations = {
            "reduce": () => {
                values.forEach(value =>  sum = value + sum );
                return res.status(200).json({"operation": operation, "result":  [sum - factor]});
            },
            "map-array-add": () => {
                values.forEach(value =>  arrayResult.push(value + factor));
                return res.status(200).json({"operation": operation, "result": arrayResult });
            },
            "map-array-mul": () => {
                values.forEach(value =>  arrayResult.push(value * factor));
                return res.status(200).json({"operation": operation, "result": arrayResult });
            }
        }

        return mathOperations[operation]();

    } catch(error) {
        return res.status(400).json({"operation": "INVALID", "result": arrayResult });
    }
})

module.exports = routes;